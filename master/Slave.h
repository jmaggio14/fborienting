
#ifndef Slave_h
#define Slave_h

#define SYNC_OUTPUT 1
#define MAX_GAIN 10

#include "Arduino.h"
#include "Tsunami_aplab.h"
#include "Button.h"

class Slave{
  public:
    /// functions
    Slave(char* sname, int id, int led, Button buttons[], Tsunami* tsunami);

    // output functions
    void queueAudio(int track_num, int gain);
    void stopAudio();

    void ledOn();
    void ledOff();
    void queueLed(bool state);
    void setLed(bool state);
    
    void setLedFromQueue();

    // hardware interaction functions
    void checkButtons();


  private:
    // output pins
    int _output;
    int _led;
    

    // buttons
    Button _enable;
    Button _light;
    Button _audio_a;
    Button _audio_b;


    // internal tracking variables
    int _id;
    bool _led_on;
    int _current_track = 1;
    Tsunami *_tsunami;
    char *_name;
    int _tagging_track;

};

#endif
