#include "Arduino.h"
#include "Button.h"


Button::Button(int pin, bool active_low){
  _pin = pin; 
  _active_low = active_low;

  // enable out pin
  if(_active_low){
    pinMode(_pin, INPUT_PULLUP);  
  }
  else {
    pinMode(_pin,INPUT);
  }

  // setup the internal state tracker
  pressed();  
}




///////////////////////////////////////////////////////
bool Button::pressed(){
  bool state = noUpdatePressed();
  _last_state = state;

  return state;
}

///////////////////////////////////////////////////////
bool Button::noUpdatePressed(){
  if(_active_low){
    return !( (bool)digitalRead(_pin) );
  }
  else{
    return (bool)digitalRead(_pin);  
  }
  
}

///////////////////////////////////////////////////////
bool Button::stateChanged(){
  if( noUpdatePressed() != _last_state ){
    return true;
  }
  return false;
}
