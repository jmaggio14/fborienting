#include "Arduino.h"
#include "Slave.h"
#include "Button.h"


// constructor
Slave::Slave(char* sname, int id, int led, Button buttons[], Tsunami* tsunami) {
  _id = id;
  _output = 9 - ((_id * 2) - 1) ;
  _led = led;
  _tsunami = tsunami;
  _name = sname;
  String strname = String(_name);
  // get the tagging track number from the slave id
  // all tracks are numbered 10 + slave_number
  _tagging_track = 10 + atoi( strname.charAt( strname.length() ) ); 

  _enable = buttons[0];
  _light =  buttons[1];
  _audio_a = buttons[2];
  _audio_b = buttons[3];

  pinMode(_led, OUTPUT);
}

void Slave::stopAudio() {
  //stop any currently playing tracks
  _tsunami->trackStop(_current_track);
  _tsunami->trackStop(_tagging_track);
}

/////////////////////////////////////////////////////////////////
void Slave::queueAudio(int track_num, int gain = 0) {
  // track we want want to play for stimulus
  Serial.print("playing ");
  Serial.print(track_num);

  Serial.print(" | on output:");
  Serial.println(_output);

  _tsunami->trackLoop(track_num, true);
  _tsunami->trackLoad(track_num, _output, false);
  //  _tsunami->trackGain(track_num, gain);
  _current_track = track_num;

  //   tagging signal - this will actually go to the DAQ
  _tsunami->trackLoop(_tagging_track, true);
//  _tsunami->trackGain(track_num, MAX_GAIN);
  _tsunami->trackLoad(_tagging_track, SYNC_OUTPUT, false);




  Serial.print(_current_track);
  Serial.println(" playing");
}

/////////////////////////////////////////////////////////////////
void Slave::ledOn() {
  setLed(HIGH);
}

/////////////////////////////////////////////////////////////////
void Slave::ledOff() {
  setLed(LOW);
}

/////////////////////////////////////////////////////////////////
void Slave::setLed(bool state) {
  // I honestly don't know why this reverse status is required, but it is as of 11/22/19 with Master3
  if ( state ) {
    digitalWrite(_led, LOW);
  }
  else {
    digitalWrite(_led, HIGH);
  }

}
/////////////////////////////////////////////////////////////////
void Slave::queueLed(bool state) {
  _led_on = state;
}
/////////////////////////////////////////////////////////////////
void Slave::setLedFromQueue() {
  setLed(_led_on);
}

/////////////////////////////////////////////////////////////////
void Slave::checkButtons() {
  // check if the slave's all enable button is pressed, and disable the whole board if necessary
  if (  _enable.stateChanged() ) {
    if ( !( _enable.pressed()) ) {
      ledOff();
      stopAudio();
      // no reason to do anymore work here if the slave is disabled
      return;
    }
  }

  /* check and queue the audio that will be played */
  if (_audio_a.stateChanged() || _audio_b.stateChanged()) {
    // convert binary button presses to decimal
    int track_code =  (_audio_b.pressed() * 2) + _audio_a.pressed();
    queueAudio(track_code);
  } // if state change

  /* determine whether or not to queue the LED */
  if (_light.stateChanged()) {
    queueLed( _light.pressed() );
  }

} // checkButtons()
