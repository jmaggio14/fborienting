#include "Tsunami_aplab.h"
#include "Slave.h"
#include "Button.h"

#define TSUNAMI1_PORT &Serial1
#define TSUNAMI2_PORT &Serial2
#define MASTER_ON_PIN 42
#define SERIAL_READ_BUF_SIZE 1024
#define MAX_COMMAND_SIZE 32

Button MASTER_ON(MASTER_ON_PIN);

// instantiate Tsunami 1
Tsunami tsunami1 = Tsunami(TSUNAMI1_PORT);
// instantiate Tsunami 2
Tsunami tsunami2 = Tsunami(TSUNAMI2_PORT);


// these pins provide power for the controller board
int CONTROLLER_VCCS[] = {43, 44, 45};
Button BUTTON_MAPS[][4] = {
  {Button(22), Button(23), Button(24), Button(25)}, // slave 1
  {Button(26), Button(27), Button(28), Button(29)}, // slave 2
  {Button(30), Button(31), Button(32), Button(33)}, // slave 3
  {Button(34), Button(35), Button(36), Button(37)}, // slave 4
  {Button(38), Button(39), Button(40), Button(41)}, // slave 5
};


// create all of our slave board objects
Slave SLAVES[] = {
  /* slave 1 */ Slave("slave1", 1, 10, BUTTON_MAPS[0], &tsunami1), // TSUNAMI 1. Slave 1's LED is disabled by setting it to an unused pin
  /* slave 2 */ Slave("slave2", 2, 2, BUTTON_MAPS[1], &tsunami1),
  /* slave 3 */ Slave("slave3", 3, 3, BUTTON_MAPS[2], &tsunami1),
  /* slave 4 */ Slave("slave4", 4, 4, BUTTON_MAPS[3], &tsunami1),
  /* slave 5 */ Slave("slave5", 1, 5, BUTTON_MAPS[4], &tsunami2), // TSUNAMI 2
  /* slave 5 */ Slave("slave6", 4, 8, BUTTON_MAPS[5], &tsunami2), // TSUNAMI 2
  /* slave 5 */ Slave("slave7", 3, 7, BUTTON_MAPS[6], &tsunami2), // TSUNAMI 2
  /* slave 5 */ Slave("slave8", 2, 6, BUTTON_MAPS[7], &tsunami2), // TSUNAMI 2


};

int NUM_SLAVES = sizeof(SLAVES) / sizeof(Slave);

void setup() {
  Serial.begin(115200);
  Serial.flush();
  tsunami1.start();
  tsunami1.setReporting(true);
  tsunami2.start();
  tsunami2.setReporting(true);

  // enable the power pins for our controller - no longer used
  for (int i = 0; i < (sizeof(CONTROLLER_VCCS) / sizeof(int)); i++) {
    int vcc_pin = CONTROLLER_VCCS[i];
    pinMode(vcc_pin, OUTPUT);
    digitalWrite(vcc_pin, HIGH);
  }

  stopAll();
  serialFlush();
  Serial.println("setup complete");
}

void loop() {
//  // DEBUGGING CODE - UNCOMMENT TO DEBUG
//  for (int i = 1; i < 9; i++) {
//    delay(100);
//    Serial.print("trying output: ");
//    Serial.println(i);
//    tsunami2.trackLoad(1, i, true);
//    //        tsunami2.trackGain(1,MAX_GAIN);
//  }
//  tsunami2.resumeAllInSync();
//  delay(5000);
//  SLAVES[0].ledOff();
//  stopAll();
//  return;

  // END DEBUGGING

  // if master on was just turned off, then we should turn off audio and leds
  if ( MASTER_ON.stateChanged() ) {
    Serial.print("master state changed\n");
    if ( !(MASTER_ON.pressed()) ) {
      stopAll();
    } // master on pressed
  } // master on state change

  // if master on still isn't pressed, then we check the serial port for commands then return the function (ie don't check buttons)
  if ( !(MASTER_ON.pressed()) ) {
    checkSerial();
  }
  // if master on is enabled, then we check for button commands
  else {
    for (int i = 0; i < NUM_SLAVES; i++) {
      SLAVES[i].checkButtons();
    }
    //initiate actions
    initiateInSync();
  }
}

///////////////////////////////////////////////////////////////////
void initiateInSync() {
  // start the audio signals first, since there will be a little lag
  tsunami1.resumeAllInSync();
  tsunami2.resumeAllInSync();
  // turn on/off the LEDs - this will occur almost immediately
  for (int i = 0; i < NUM_SLAVES; i++) {
    SLAVES[i].setLedFromQueue();
  }
}

///////////////////////////////////////////////////////////////////
void stopAll() {
  for (int i = 0; i < NUM_SLAVES; i++) {
    SLAVES[i].ledOff();
  } // slave for loop
  tsunami1.stopAllTracks();
  tsunami2.stopAllTracks();
}
///////////////////////////////////////////////////////////////////
void serialFlush() {
  // flush the remaining buffer
  while ( Serial.available() ) {
    Serial.read();
  }
}
///////////////////////////////////////////////////////////////////
void checkSerial() {
  // return if we have no incoming bytes
  if (!(Serial.available())) {
    return;
  }

  //  ///////////////////////////////////////////////////////
  while ( Serial.available() ) {
    String msg = Serial.readStringUntil('\n');
    msg.replace(" ", ""); //remove all whitespace
    msg.toUpperCase(); // make everything uppercase

    Serial.println(msg);

    // check if we have a full stop command before we parse slave commands
    if ( msg.equals("FULLSTOP") ) {
      stopAll();
      serialFlush();
      return;
    }
    int slave_idx = msg.substring(0, 1).toInt() - 1;
    String action = msg.substring(1, 6);
    int option = msg.substring(6, 7).toInt();


    Serial.print("slave_id:");
    Serial.println(slave_idx + 1);

    Serial.print("action:");
    Serial.println(action);

    Serial.print("option:");
    Serial.println(option);


    // Audio Track
    if ( action.equals("TRACK") ) {
      if ( option == 0) {
        SLAVES[slave_idx].stopAudio();
      }
      else {
        SLAVES[slave_idx].queueAudio(option, 0);
      }
    }//end audio track

    else if ( action.equals("LIGHT") ) {
      SLAVES[slave_idx].queueLed( (bool)option );
    }
  } // while there are bytes in the incoming buffer
  initiateInSync();
  serialFlush();

}
