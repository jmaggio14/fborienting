

#ifndef Button_h
#define Button_h

#include "Arduino.h"

class Button{
  public:
    Button(int pin=0, bool active_low=true); // default constructor
    bool noUpdatePressed();
    bool pressed();
    bool stateChanged();

  private:
    int _pin;
    int _last_state;
    bool _active_low;

  
};


#endif
